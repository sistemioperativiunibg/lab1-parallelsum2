package it.unibg.so.lab1;

import java.util.List;
import java.util.concurrent.Callable;

public class SumTask extends Thread implements Callable<Integer> {
	
	private List<Integer> values;
	private int id;
	
	public SumTask(int id, List<Integer> values){
		this.values = values;
		this.id = id;
	}

	@Override
	public Integer call() throws Exception {
		System.out.println("[Task-" + id + "] computing...");
		int sum = 0;
		for(Integer n: values)
			sum += n;
		return sum;
	}

}
